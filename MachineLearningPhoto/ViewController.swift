//
//  ViewController.swift
//  MachineLearningPhoto
//
//  Created by Jorge Encinas on 9/25/19.
//  Copyright © 2019 Jorge Encinas. All rights reserved.
//

import UIKit
import Vision
import CoreML

class ViewController: UIViewController {
    
    @IBOutlet weak var dataImage: UIImageView!
    @IBOutlet weak var dataLabel: UILabel!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.detecImage()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func takePicture(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else{
            print("No se puede acceder a la camara")
        }
    }
    
    @IBAction func selectPicture(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            print("No se puede acceder a las fotos")
        }
    }
    
    // MARK: Functions
    
    func detecImage(){
        self.dataLabel.text = "Cargando..."
        print("Starting...")
        guard let model =  try? VNCoreMLModel(for: GoogLeNetPlaces().model) else {
            print(" :( Error building the model...")
            return
        }
        
        let petition = VNCoreMLRequest(model: model) { (request, error) in
            guard let results = request.results as? [VNClassificationObservation], let firstResult = results.first else{
                print("No se encontraron resultados")
                return
            }
            print("Resultados: \(results.count)")
            DispatchQueue.main.async {
                print("Got first result")
                self.dataLabel.text = "\(firstResult.identifier)  Confidence: \((firstResult.confidence * 100).roundedCIOK(toPlaces: 0))%"
            }
            
        }
        
        guard let ciImageForUse = CIImage(image: self.dataImage.image!) else{
            print("No se ha podido cargar imagen")
            return
        }
        
        let handler = VNImageRequestHandler(ciImage: ciImageForUse)
        
        DispatchQueue.global().async {
            do{
                print("Performing")
                try handler.perform([petition])
            }catch{
                print("Error: \(error.localizedDescription)")
            }
        }
        
    }
    
    
}

extension ViewController: UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            
            self.dataImage.contentMode = .scaleAspectFill
            self.dataImage.image = pickedImage
        }
        
        picker.dismiss(animated: true, completion: nil)
        detecImage()
        
    }
}

extension ViewController: UINavigationControllerDelegate{
    
}

extension Float {
    func roundedCIOK(toPlaces places: Int) -> String {
        let format = NumberFormatter()
        format.maximumFractionDigits = places
        format.roundingMode = .up
        return format.string(from: NSNumber(value: self)) ?? ""
    }
}
