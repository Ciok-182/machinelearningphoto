import coremltools
from sklearn.linear_model import LinearRegression
import pandas

primaryData = pandas.read_csv("HouseSalesInCA.csv")

modelo = LinearRegression()
modelo.fit(primaryData[["Bedrooms","Bathrooms","Size"]],primaryData["Price"])

coremlModel = coremltools.converters.sklearn.convert(modelo, ["Bedrooms","Bathrooms","Size"], "Price")

coremlModel.author = "Jorge Encinas"
coremlModel.short_description = "Modelo para calcular el precio de una casa dependiendo del numero de habitaciones banios y tamanio del terreno en el estado de california"
coremlModel.license = "MIT"


coremlModel.save("HouseSalesInCA.mlmodel")